# sql query builder

- [ ] implement webapp to run sql query and display result

## personas

**data analyst**

- analyze data to make decisions
  - lets say in an IKEA store there are customer who brought a "TABLE" and there is data on purchase of "TABLE LAMP" brought variably distanced from "TABLE", how close would you place it for higher conversion ?
  - [Example](https://github.com/chop-analytics/analyst-take-home-task)
  - [Example](https://github.com/akaboshi900306/Shipt-Data-Analyst-Assignment/blob/master/Take%20Home%20Problem%20-%20Python%20-%20Analyst.pdf)
- tools used to visualize data
  - colab, Jupyter notebooks, observable etc,

## Implementation

- Must

  - [ ] space to accept sql query and display result (relatively grouped)
  - [ ] more than 1 query
  - [ ] Maybe predefined query (for display)
  - [ ] toggle between query, choose any query and view table

- Not required

  - query input needs no validation / syntax highlight
  - no backend / query engine
  - data need not be synced with the query (a load indicator is enough)

- assumptions

  - a data analyst would pull in data from different data sources, cleans them and plot charts. For this application we are only working relating different query in a page, renders only tables from different query

## Feature set

- Basic
  - [ ] Pages
    - Title
    - A description box for data question
    - a group of all the query related to answer that question
    - a stable url to able to share this page
- Nice to have
  - [ ] keyboard shortcuts
    - use the app entirely from keyboard (hotkey)

import { customAlphabet, urlAlphabet } from "nanoid";

export function NewId() {
  return customAlphabet(urlAlphabet, 12)();
}

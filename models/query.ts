export interface IQuery {
  // url safe id
  id: number;
  // should be rendered after, -1 indicates first
  sort_after: number;
  name: string;
  query: string;
}

export function NewQuery(id = 1): IQuery {
  return {
    id: id,
    sort_after: -1,
    name: `query-${id}`,
    query: "",
  };
}

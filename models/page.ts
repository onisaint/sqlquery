import { IQuery, NewQuery } from "./query";
import { NewId } from "./id";

export interface IPage {
  // url safe id
  id: string;
  title: string;
  description: string;
  queries: Map<number, IQuery>;
  created_at: Date;
}

export function NewPage(): IPage {
  const newQuery = NewQuery(1);

  return {
    id: NewId(),
    title: "Untitled",
    description: "",
    queries: new Map([[newQuery.id, newQuery]]),
    created_at: new Date(),
  };
}

import { useContext, useMemo } from "react";
import { StoreCtx } from "./store.provider";

export const usePages = () => {
  const {
    pageStore: {
      pages,
      sortedPages,
      getAllQueriesForPage,
      create,
      update,
      remove,
    },
  } = useContext(StoreCtx);

  return {
    pages,
    sortedPages,
    getAllQueriesForPage,
    $create: create,
    $update: update,
    $remove: remove,
  };
};

export const useQueries = () => {
  const {
    pageStore: {
      queryStore: { create, update, remove },
    },
  } = useContext(StoreCtx);

  return {
    $create: create,
    $update: update,
    $remove: remove,
  };
};

/**
 * return the page with sorted queries for the given pageID
 * @param pageID
 * @returns
 */
export const useOnePage = (pageID: string) => {
  const { pages, getAllQueriesForPage } = usePages();

  return useMemo(() => {
    const page = pages.get(pageID);
    if (!page) return null;

    const currentPage = { ...page };

    currentPage.queries = getAllQueriesForPage(page.id);
    return currentPage;
  }, [pages, pageID]);
};

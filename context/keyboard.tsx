import { createContext, FC, ReactNode, useContext, useMemo } from "react";
import { fromEvent, Subject } from "rxjs";

type IKeySubjects = Subject<KeyboardEvent>;

const KeyboardCtx = createContext<IKeySubjects>({} as IKeySubjects);

interface IProps {
  children: ReactNode;
}

export const KeyboardEventProvider: FC<IProps> = ({ children }) => {
  const value: IKeySubjects = useMemo(() => {
    const events = new Subject<KeyboardEvent>();

    fromEvent<KeyboardEvent>(document, "keydown").subscribe({
      next: (e) => {
        const t = e.target as HTMLElement;
        if (t.contentEditable === "true") {
          return;
        }

        if (["INPUT", "TEXTAREA"].includes(t.nodeName)) {
          return;
        }

        events.next(e);
      },
    });

    return events;
  }, []);

  return <KeyboardCtx.Provider value={value}>{children}</KeyboardCtx.Provider>;
};

export const useKeyboardEvents = (): IKeySubjects => useContext(KeyboardCtx);

import { FC, ReactNode, createContext } from "react";
import { Store } from "../store";

export const StoreCtx = createContext<Store>({} as Store);

interface IProps {
  store: Store;
  children: ReactNode;
}

export const StoreProvider: FC<IProps> = ({ store, children }) => {
  return <StoreCtx.Provider value={store}>{children}</StoreCtx.Provider>;
};

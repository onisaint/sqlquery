# sqlquery

an simple app to group together queries to answer analytical questions, inspired by the likes of colab, Jupyter notebooks, https://onisaint.gitlab.io/sqlquery/

## overview

the project lets you create pages grouping queries, execute and switch between queries - all from your keyboard

## optimizations

    - react-intersection-observer
        - query table are only loaded once it comes into the visible range and stays visible until unmounted
    - tanstack/tables & react-virtual (2.x)
        - initially thought of creating an observer window with million.js, due to time constraints decide to use this
    - react.useTransition api
        - render and update table data without blocking ui
    - rxjs.fromEvent
        - implementing the entire app to use one keyboard event observer

## packages

~ 100kb of gzip js production

    - react (with vite)
        - styled-components
        - react-router
        - nanoid (ids)
        - react-intersection-observer
        - is-hotkey
        - rxjs
        - cmdk
        - react-virtual
        - tanstack/tables
    - store
        - mobx
    - dev-tools
        - vitest (test)
        - tiny-invariant
        - web-vitals

## metrics (on production build)

    - page speed (first paint)
        - localhost
            - .33s
        - 3G, 6xSlowdown (with web-vitals)
            - 5.1s

    - web-vitals
        - first content paint: good
        - layout shift: 0 (good)
        - interaction paint: good
        - input delay: user dependent (for tab, good) no rendering block
        - first byte: good

    - afterframe (test frame change speed) - skipped

## assumptions and notes

    - in theory around 1000 rows are generated, and the table at any point contains ~24 children, it should handle any nu. of rows even a million
        - while testing the browser would hang for creating 10000 rows at once (no throttling) and around 1000 with cpu throttling
    -  skipped on loading a csv / any data source in file, used a stub function instead
        - to load a million rows ideally we need a query backend, loading in file is ~10mb (generated csv), any way the challenge did not prioritize correctness of data
    - each time a sql input box is interacted, the table changes, just for simulation, ideally this would be cached.
    - no pwa. simple app, browser default cache should be enough
    - the app is not optimized for mobile view, assuming this is dev tool and would be used on desktop
    - did not use any svg, images or additional fonts , design is not prioritized

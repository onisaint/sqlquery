import { styled } from "styled-components";

export const H1 = styled.h1`
  color: #333;
  font-size: var(--font-big);
  line-height: 1.4;
`;

export const SmallHeading = styled.label`
  text-align: left;
  color: #323232;
  font-size: var(--font-label);
  line-height: 1.4;
`;

export const Para = styled.p`
  text-align: left;
  margin: 0;
  font-size: var(--font-para);
  line-height: 1.4;
`;

export const Small = styled.small`
  text-align: left;
  margin: 0;
  font-size: var(--font-small);
  line-height: 1.4;
`;

export const ParaMono = styled.p`
  text-align: left;
  margin: 0;
  font-family: var(--mono);
  font-size: var(--font-label);
  line-height: 1.4;
`;

export const KeyboardShortcut = styled(ParaMono)`
  border: 1px solid #f1f1f1;
  padding: 2px 4px;
  border-radius: 3px;
`;

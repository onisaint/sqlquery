import { css, styled } from "styled-components";

export const InputStyles = css`
  padding: 2px 4px;
  margin: 0;
  border: none;
  border-radius: 3px;
  height: fit-content;
  transition: background 0.17s ease;
  line-height: 1.4;
  width: 100%;

  &:hover,
  &:focus {
    background: #f1f1f1;
  }

  outline: none;
`;

export const Input = styled.input`
  ${InputStyles};
`;

export const HeadingInput = styled.input`
  ${InputStyles};
  font-family: var(--serif);
  font-size: var(--font-big);
  font-weight: bold;
`;

export const DescriptionArea = styled.textarea`
  ${InputStyles};
  font-family: var(--serif);
  font-size: var(--font-para);
  min-height: calc(18px * 5);
  max-height: fit-content:
  overflow: hidden;
`;

export const SQLInput = styled.textarea`
  ${InputStyles};
  height: fit-content;
  max-height: calc(18px * 4);
  font-family: var(--mono);
  font-size: var(--font-para);
`;

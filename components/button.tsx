import { css, styled } from "styled-components";

const ButtonStyles = css`
  padding: 2px 4px;
  border: none;
  background: none;
  cursor: pointer;
  border-radius: 3px;
  background: #f1f1f1;
  transition: background 0.17s ease;

  &:hover,
  &:focus {
    background: #f4f4f4;
  }
`;

export const Button = styled.button`
  ${ButtonStyles};
  display: flex;
  align-items: center;
  justify-content: space-between;

  & > p {
    color: #333;
    font-size: var(--font-para);
  }
`;

export const LinkButton = styled.button`
  ${ButtonStyles};
  display: flex;
  align-items: center;
  justify-content: space-between;

  & > p {
    color: #333;
    font-size: var(--font-small);
  }
`;

interface INavButtonLinkProps {
  selected: boolean;
}

export const NavButtonLink = styled.button<INavButtonLinkProps>`
  ${ButtonStyles};
  display: block;
  padding: 2px 0;
  background: transparent;
  opacity: 0.7;
  transition: opacity 0.17s ease;
  position: relative;

  &:hover,
  &:focus {
    background: transparent;
    opacity: 1;
  }

  ${({ selected }) => {
    if (selected) {
      return css`
        opacity: 1;

        &::before {
          content: "|";
          position: absolute;
          left: -8px;
          top: 4px;
          font-size: var(--font-para);
        }
      `;
    }
  }}
`;

import { styled } from "styled-components";

export const Frame = styled.div`
  margin: 16px auto;

  display: grid;
  grid-template-columns: 200px 1fr;
  grid-template-areas: "nav page";

  @media (min-width: 1000px) {
    max-width: 1000px;
  }

  @media (min-width: 1400px) {
    max-width: 1400px;
  }
`;

export const FrameNav = styled.div`
  grid-area: nav;
  position: fixed;
  top: 0;
  bottom: 0;
  width: 200px;
  margin: 16px 0;
`;

export const FramePage = styled.div`
  grid-area: page;
  margin: 0 16px;
  display: block;
`;

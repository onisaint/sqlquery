import { PageStore } from "./page";

export class Store {
  pageStore: PageStore;

  constructor() {
    this.pageStore = new PageStore();
  }
}

import { action, computed, makeObservable, observable } from "mobx";
import { IPage, NewPage } from "../models/page";
import { DeleteMapValue, UpdateMap } from "./helper";
import { QueryStore, sortQueries } from "./query";
import invariant from "tiny-invariant";
import { IQuery } from "../models/query";

export class PageStore {
  pages: Map<string, IPage> = new Map<string, IPage>();

  queryStore: QueryStore;

  constructor() {
    makeObservable(this, {
      pages: observable,
      sortedPages: computed,
      create: action,
      update: action,
      remove: action,
    });

    this.queryStore = new QueryStore(this);
  }

  // get all sorted pages
  get sortedPages(): IPage[] {
    return Array.from(this.pages.values()).sort((a, b) => {
      return a.created_at.getTime() - b.created_at.getTime();
    });
  }

  // get sorted queries for the page
  getAllQueriesForPage = (pageId: string) => {
    const page = this.pages.get(pageId);
    invariant(page, "The page you are looking for is not found!");

    return sortQueries(Array.from(page.queries.values()));
  };

  // a new "untitled" page
  createFromData = (
    title: string,
    description: string,
    queries: Pick<IQuery, "name" | "query">[]
  ) => {
    const newPage = { ...NewPage(), title, description };
    this.pages = UpdateMap(this.pages, newPage.id, newPage);

    this.queryStore.remove(newPage.id, 1);

    let lastQueryId = -1;
    for (const q of queries) {
      lastQueryId = this.queryStore.createFromData(newPage.id, q, lastQueryId);
    }
  };

  create = (): IPage => {
    const newPage = NewPage();
    this.pages = UpdateMap(this.pages, newPage.id, newPage);
    return newPage;
  };

  update = (id: string, params: Omit<IPage, "id">) => {
    this.pages = UpdateMap(this.pages, id, { id, ...params });
  };

  remove = (id: string) => {
    this.pages = DeleteMapValue(this.pages, id);

    if (this.pages.size === 0) {
      this.create();
    }
  };
}

/**
 * create a new Map of the current and update value
 *
 * @param m old map
 * @param id id for update
 * @param value what to update with
 * @returns new Map(m)
 */
export function UpdateMap<K, V>(m: Map<K, V>, id: K, value: V) {
  const newMap = new Map(m);
  newMap.set(id, value);
  return newMap;
}

/**
 * create a new Map of the current and delete value
 * @param m old map
 * @param id id for delete
 * @returns new Map(m)
 */
export function DeleteMapValue<K, V>(m: Map<K, V>, id: K) {
  const newMap = new Map(m);
  newMap.delete(id);
  return newMap;
}

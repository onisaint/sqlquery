import { describe, test, beforeEach, expect } from "vitest";
import { IQuery, NewQuery } from "../../models/query";
import { addQuery, deleteQuery, sortQueries } from "../query";

describe("addQuery() should return queries linking new", () => {
  let queries: IQuery[] = [];

  beforeEach(() => {
    queries = [NewQuery(1), { ...NewQuery(2), sort_after: 1 }];
  });

  test("add new query at the end for no sortAfterId", () => {
    const newQuery = NewQuery(3);
    const addedQueries = addQuery(queries, newQuery);

    expect(addedQueries[2].id).toBe(newQuery.id);
    expect(addedQueries[2].sort_after).toBe(addedQueries[1].id);
  });

  test("add new query at the start for sortAfterId -1", () => {
    const newQuery = NewQuery(3);
    const addedQueries = addQuery(queries, newQuery, -1);

    expect(addedQueries[2].id).toBe(newQuery.id);
    expect(addedQueries[2].sort_after).toBe(-1);
    expect(addedQueries[0].sort_after).toBe(newQuery.id);
  });

  test("add new with a sortAfterId", () => {
    const newQuery = NewQuery(3);
    const addedQueries = addQuery(queries, newQuery, 1);

    expect(addedQueries[2].id).toBe(newQuery.id);
    expect(addedQueries[2].sort_after).toBe(addedQueries[0].id);
    expect(addedQueries[1].sort_after).toBe(newQuery.id);
  });
});

describe("deleteQuery() should remove a query and patch the broken link", () => {
  let queries: IQuery[] = [];

  beforeEach(() => {
    queries = [
      NewQuery(1),
      { ...NewQuery(2), sort_after: 1 },
      { ...NewQuery(3), sort_after: 2 },
    ];
  });

  test("delete an undefined / unrelated query should return the same", () => {
    const sortedQuery = deleteQuery(queries, NewQuery(20));

    expect(expect.arrayContaining(sortedQuery)).toEqual(queries);
  });

  test("delete the ˝first should return the last 2 and 0th.sortAfter to -1", () => {
    const sortedQuery = deleteQuery(queries, queries[0]);

    expect(sortedQuery.length).toBe(2);
    expect(sortedQuery[0]).toBe(queries[1]);
    expect(sortedQuery[0].sort_after).toBe(-1);
    expect(sortedQuery[1]).toBe(queries[2]);
    expect(sortedQuery[1].sort_after).toBe(queries[1].id);
  });

  test("delete the last query should return the first 2", () => {
    const sortedQuery = deleteQuery(queries, queries[2]);

    expect(sortedQuery.length).toBe(2);
    expect(sortedQuery[0]).toBe(queries[0]);
    expect(sortedQuery[0].sort_after).toBe(-1);
    expect(sortedQuery[1]).toBe(queries[1]);
    expect(sortedQuery[1].sort_after).toBe(queries[0].id);
  });

  test("delete the middle query should return patched query", () => {
    const sortedQuery = deleteQuery(queries, queries[1]);

    expect(sortedQuery.length).toBe(2);
    expect(sortedQuery[0]).toBe(queries[0]);
    expect(sortedQuery[0].sort_after).toBe(-1);
    expect(sortedQuery[1]).toBe(queries[2]);
    expect(sortedQuery[1].sort_after).toBe(queries[0].id);
  });
});

test("sortQueries() should chain link the queries or random order returning sortedMap of queries", () => {
  const queries = [
    { ...NewQuery(3), sort_after: 2 },
    { ...NewQuery(2), sort_after: 1 },
    NewQuery(1),
  ];

  const sorted = sortQueries(queries);

  const sortedArray = Array.from(sorted.values());

  expect(sortedArray[0].id).toBe(1);
  expect(sortedArray[0].sort_after).toBe(-1);
  expect(sortedArray[1].id).toBe(2);
  expect(sortedArray[1].sort_after).toBe(sortedArray[0].id);
  expect(sortedArray[2].id).toBe(3);
  expect(sortedArray[2].sort_after).toBe(sortedArray[1].id);
});

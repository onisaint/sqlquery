import { beforeEach, describe, expect, test } from "vitest";
import { Store } from "..";
import { PageStore } from "../page";

describe("pageStore", () => {
  let ps: PageStore;

  beforeEach(() => {
    const s = new Store();
    ps = s.pageStore;
  });

  test(".create() should create a new Page with 1 query", () => {
    const newPage = ps.create();

    expect(ps.pages.size).toBe(1);
    expect(newPage.title).toBe("Untitled");
    expect(newPage.queries.size).toBe(1);

    const query = newPage.queries.get(1);

    expect(query?.id).toBe(1);
    expect(query?.sort_after).toBe(-1);
  });

  test(".update() should update current page", () => {
    const newPage = ps.create();

    expect(newPage.title).toBe("Untitled");
    expect(newPage.description).toBe("");

    ps.update(newPage.id, { ...newPage, title: "abc", description: "123" });

    const updatePage = ps.pages.get(newPage.id);
    expect(updatePage?.title).toBe("abc");
    expect(updatePage?.description).toBe("123");
  });

  test(".remove() should delete a page", () => {
    ps.create();
    const p2 = ps.create();
    ps.create();

    expect(ps.pages.size).toBe(3);

    ps.remove(p2.id);

    expect(ps.pages.size).toBe(2);
    expect(ps.pages.get(p2.id)).toBeUndefined();
  });
});

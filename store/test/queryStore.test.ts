import { beforeEach, describe, expect, test } from "vitest";
import { Store } from "..";
import { QueryStore } from "../query";
import { PageStore } from "../page";

describe("queryStore", () => {
  let ps: PageStore;
  let qs: QueryStore;

  beforeEach(() => {
    const s = new Store();
    ps = s.pageStore;
    qs = s.pageStore.queryStore;
  });

  test(".create() creates a newQuery on page with index q1.id + 1", () => {
    const newPage = ps.create();
    qs.create(newPage.id);

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(2);

    const q1 = ps.pages.get(newPage.id)?.queries.get(1);
    expect(q1?.id).toBe(1);
    expect(q1?.sort_after).toBe(-1);

    const q2 = ps.pages.get(newPage.id)?.queries.get(2);
    expect(q2?.id).toBe(2);
    expect(q2?.sort_after).toBe(q1?.id);
  });

  test(".create() creates a query with id 1 on a page with 0 queries", () => {
    const newPage = ps.create();
    // delete the first query
    qs.remove(newPage.id, 1);

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(0);

    const q1 = qs.create(newPage.id);

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(1);

    expect(q1?.id).toBe(1);
    expect(q1?.sort_after).toBe(-1);
  });

  test(".create() after a random .create and .remove op on queries, a new query should have an id to be max+1", () => {
    const newPage = ps.create();
    // create 10 queries
    for (let i = 0; i < 10; i++) {
      qs.create(newPage.id);
    }

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(11);

    qs.remove(newPage.id, 1);
    qs.remove(newPage.id, 4);
    qs.remove(newPage.id, 7);

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(8);

    const newQuery = qs.create(newPage.id);

    expect(newQuery.id).toBe(12);
    expect(newQuery.sort_after).toBe(11);
  });

  test(".update() should update a query", () => {
    const newPage = ps.create();
    const newQuery = qs.create(newPage.id);

    let q1 = ps.pages.get(newPage.id)?.queries.get(newQuery.id);
    expect(q1?.name).toBe("query-2");
    expect(q1?.query).toBe("");

    const name = "abc";
    const sqlQuery = "select *";
    qs.update(newPage.id, newQuery.id, {
      ...newQuery,
      name,
      query: sqlQuery,
    });

    q1 = ps.pages.get(newPage.id)?.queries.get(newQuery.id);
    expect(q1?.name).toBe(name);
    expect(q1?.query).toBe(sqlQuery);
  });

  test(".remove() should remove a query", () => {
    const newPage = ps.create();
    // create 10 queries
    for (let i = 0; i < 10; i++) {
      qs.create(newPage.id);
    }

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(11);

    qs.remove(newPage.id, 4);

    expect(ps.pages.get(newPage.id)?.queries.size).toBe(10);

    expect(ps.pages.get(newPage.id)?.queries.get(4)).toBeUndefined();
  });
});

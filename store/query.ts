import invariant from "tiny-invariant";
import { PageStore } from "./page";
import { IQuery, NewQuery } from "../models/query";
import { runInAction } from "mobx";

export class QueryStore {
  static sort = sortQueries;

  private pageStore: PageStore;

  constructor(pageStore: PageStore) {
    this.pageStore = pageStore;
  }

  /**
   * creates a query updates and returns
   *
   * @param pageId
   * @param queryData
   * @param sortAfterId
   * @returns new query id
   */
  createFromData = (
    pageId: string,
    queryData: Omit<IQuery, "id" | "sort_after">,
    sortAfterId = -99
  ) => {
    const newQuery = this.create(pageId, sortAfterId);
    this.update(pageId, newQuery.id, queryData);
    return newQuery.id;
  };

  /**
   * creates a new query, sort and save
   *
   * @param pageId query relative to the page
   * @param relativeQueryId a relative query id to append the new one, -1 to place at top, if not given will be added to last
   * @returns new query
   */
  create = (pageId: string, sortAfterId = -99) => {
    const page = this.pageStore.pages.get(pageId);
    invariant(page, "The page you are looking for is not found!");

    const queries = Array.from(page.queries.values());

    let nextId = 1;
    if (queries.length > 0) {
      nextId = Math.max(...queries.map((q) => q.id)) + 1;
    }

    const newQuery = NewQuery(nextId);

    runInAction(() => {
      page.queries = sortQueries(addQuery(queries, newQuery, sortAfterId));
      this.pageStore.update(page.id, page);
    });

    return newQuery;
  };

  update = (
    pageId: string,
    queryId: number,
    params: Omit<IQuery, "id" | "sort_after">
  ) => {
    const page = this.pageStore.pages.get(pageId);
    invariant(page, "The page you are looking for is not found!");

    const query = page.queries.get(queryId);
    invariant(query, "The query you are looking for is not found!");

    runInAction(() => {
      page.queries.set(queryId, { ...query, ...params });
      this.pageStore.update(pageId, page);
    });
  };

  /**
   * delete a query and sort the query
   *
   * @param pageId query relative to the page
   * @param queryId
   */
  remove = (pageId: string, queryId: number) => {
    const page = this.pageStore.pages.get(pageId);
    invariant(page, "The page you are looking for is not found!");

    const query = page.queries.get(queryId);
    invariant(query, "The query you are looking for is not found!");

    const queries = Array.from(page.queries.values());

    runInAction(() => {
      page.queries = sortQueries(deleteQuery(queries, query));
      this.pageStore.update(page.id, page);
    });
  };
}

/**
 * add new query to the current query and
 * point it to the relevant sort_after
 *
 * @note it doesn't sort the query, just links the ids and pushes to the end
 *
 * @param queries
 * @param newQuery
 * @param sortAfterId
 * @returns
 */
export function addQuery(
  queries: IQuery[],
  newQuery: IQuery,
  sortAfterId = -99
): IQuery[] {
  if (queries && queries.length > 0) {
    let isSortedOnPrecondition = false;
    // sort after the last query
    if (sortAfterId === -99) {
      newQuery.sort_after = (queries.at(-1) as IQuery).id;
      isSortedOnPrecondition = true;
    }

    // element is asked to pace first ? then first element sortAfterId is the current Id
    if (sortAfterId === -1) {
      const firstQueryIndex = queries.findIndex((q) => q.sort_after === -1);
      if (firstQueryIndex > -1) {
        queries[firstQueryIndex].sort_after = newQuery.id;
      }
      isSortedOnPrecondition = true;
    }

    if (!isSortedOnPrecondition) {
      const relativeQuery = queries.find((q) => q.id === sortAfterId);
      invariant(relativeQuery, "The query you are looking for is not found!");

      // sort after element
      newQuery.sort_after = relativeQuery.id;

      // chain queries after relative queries to sort after new query
      const queryAfterRelativeQueryIndex = queries.findIndex(
        (q) => q.sort_after === relativeQuery.id
      );
      if (queryAfterRelativeQueryIndex !== -1) {
        queries[queryAfterRelativeQueryIndex].sort_after = newQuery.id;
      }
    }
  }
  queries.push(newQuery);
  return queries;
}

/**
 * deletes a query and
 * chain the old_query.sort_after -> query.id to query.sort_after
 *
 * @param queries
 * @param query query to delete
 * @returns
 */
export function deleteQuery(queries: IQuery[], query: IQuery) {
  const sorted = queries.filter((q) => q.id !== query.id);

  if (sorted.length !== 0) {
    const nextIndex = sorted.findIndex((q) => q.sort_after === query.id);

    if (nextIndex !== -1) {
      sorted[nextIndex].sort_after = query.sort_after;
    }
  }

  return sorted;
}

/**
 * creates a Record of <query.sort_after, query>,
 * from -1 lookup next id which should come after
 *
 * @param queries
 * @returns sorted new Map(queries)
 */
export function sortQueries(queries: IQuery[]): Map<number, IQuery> {
  const sorted: IQuery[] = [];

  // keys cannot be missed
  const queryLookUpMap = queries.reduce<Record<number, IQuery>>(
    (c, n) => ({ ...c, [n.sort_after]: n }),
    {}
  );

  let next: IQuery | undefined = queryLookUpMap[-1];
  while (next) {
    sorted.push(next);
    next = queryLookUpMap[next.id];
  }

  return new Map(sorted.map((s) => [s.id, s]));
}

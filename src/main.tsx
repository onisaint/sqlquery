import React from "react";
import ReactDOM from "react-dom/client";
import { Store } from "../store/index.ts";
import { StoreProvider } from "../context/store.provider.tsx";
import { BrowserRouter } from "react-router-dom";
import { AppRoutes } from "./routes.tsx";
import { KeyboardEventProvider } from "../context/keyboard.tsx";

const store = new Store();

// create a default page
store.pageStore.createFromData(
  "Maximize table lamp sales",
  'lets say in an IKEA store there are customer who brought a "TABLE" and there is data on purchase of "TABLE LAMP" brought variably distanced from "TABLE", how close would you place it for higher conversion ?\n\nps: this is dummy. the queries and tables are not related at all 😁',
  [
    {
      name: "table sales",
      query: "select * from table_sales",
    },
    {
      name: "table lamp sales",
      query: "select * from table_lamp_sales",
    },
    {
      name: "table locations",
      query: "select * from table_location",
    },
    {
      name: "table lamp locations",
      query: "select * from table_lamp_location",
    },
    {
      name: "get the min location with max sales",
      query:
        "select min(TL.distance - TLL.distance) from table_locations as TL\njoin table_lamp_location as TLL on TL.store_id = TLL.store_id",
    },
  ]
);
store.pageStore.create();

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <KeyboardEventProvider>
      <StoreProvider store={store}>
        <BrowserRouter>
          <AppRoutes />
        </BrowserRouter>
      </StoreProvider>
    </KeyboardEventProvider>
  </React.StrictMode>
);

// ideally should store and push to a metric server
// logging to console for now
void (async () => {
  const vitals = await import("./vitals");
  vitals.default();
})();

let base = "";
if (import.meta.env.PROD) {
  base = "/sqlquery";
}

export const makePageUrl = (pageId: string) => `${base}/pages/${pageId}`;

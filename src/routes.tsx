import { FC, useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { usePages } from "../context/store";
import { View } from "./view";
import { makePageUrl } from "./helper";

let base = "";
if (import.meta.env.PROD) {
  base = "/sqlquery";
}

const RedirectDefault = () => {
  const navigate = useNavigate();

  const { sortedPages } = usePages();

  useEffect(() => {
    navigate(makePageUrl(sortedPages[0].id));
  });

  return null;
};

export const AppRoutes: FC = () => {
  return (
    <Routes>
      <Route path={`${base}/pages/:pageID`} element={<View />} />
      <Route path="*" element={<RedirectDefault />} />
    </Routes>
  );
};

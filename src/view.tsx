import { Frame } from "../components/frame";
import { Pages } from "./pages";
import { PageList } from "./pages/list";

export const View = () => {
  return (
    <Frame>
      <PageList />
      <Pages />
    </Frame>
  );
};

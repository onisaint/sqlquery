import { FocusQueryProvider } from "./context";
import { PageView } from "./view";

export const Pages = () => {
  return (
    <FocusQueryProvider>
      <PageView />
    </FocusQueryProvider>
  );
};

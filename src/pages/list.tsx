import { FC, useEffect } from "react";
import { observer } from "mobx-react-lite";
import { Para, ParaMono, SmallHeading } from "../../components/types";
import { styled } from "styled-components";
import { usePages } from "../../context/store";
import { LinkButton, NavButtonLink } from "../../components/button";
import { FrameNav } from "../../components/frame";
import { useNavigate, useParams } from "react-router-dom";
import { makePageUrl } from "../helper";
import { useKeyboardEvents } from "../../context/keyboard";
import isHotkey from "is-hotkey";

const ListItem = styled(Para)`
  font-family: var(--serif);
  margin-top: 4px;
`;

const AddPageButton = styled(LinkButton)`
  margin-top: 20px;
`;

export const PageList: FC = observer(() => {
  const { pageID } = useParams() as { pageID: string };
  const navigate = useNavigate();

  const keyboardEvent = useKeyboardEvents();

  const { sortedPages, $create } = usePages();

  const onNavigateToPage = (pageId: string) => () => {
    navigate(makePageUrl(pageId));
  };

  const onAddPage = () => {
    const page = $create();
    onNavigateToPage(page.id)();
  };

  useEffect(() => {
    const _s = keyboardEvent.subscribe((e) => {
      if (isHotkey("shift+n", e)) {
        onAddPage();
      }
    });

    return () => _s.unsubscribe();
  }, [keyboardEvent]);

  useEffect(() => {
    if (!sortedPages.some((p) => p.id === pageID)) {
      onNavigateToPage(sortedPages[0].id)();
    }
  }, [sortedPages, pageID]);

  return (
    <FrameNav>
      <SmallHeading>Your pages</SmallHeading>
      {sortedPages.map((p) => (
        <NavButtonLink
          key={p.id}
          selected={p.id === pageID}
          onClick={onNavigateToPage(p.id)}
        >
          <ListItem>{p.title}</ListItem>
        </NavButtonLink>
      ))}
      <AddPageButton onClick={onAddPage}>
        <Para>Add a page</Para>
        <ParaMono className="ml-8">⇧N</ParaMono>
      </AddPageButton>
    </FrameNav>
  );
});

import { observer } from "mobx-react-lite";
import isHotkey from "is-hotkey";
import { FC, FocusEvent, useEffect, useRef } from "react";
import { useParams } from "react-router-dom";
import { Button } from "../../components/button";
import { Row, RowSpaced } from "../../components/flex";
import { FramePage } from "../../components/frame";
import { DescriptionArea, HeadingInput } from "../../components/input";
import { KeyboardShortcut, Para, ParaMono } from "../../components/types";
import { useOnePage, usePages, useQueries } from "../../context/store";
import { Query } from "./query";
import { useKeyboardEvents } from "../../context/keyboard";
import { useFocusQuery } from "./context";

export const PageView: FC = observer(() => {
  const { focusTo } = useFocusQuery();

  const { pageID } = useParams();

  const keyboardEvent = useKeyboardEvents();

  const pageRef = useRef<HTMLDivElement>(null);

  const { $update, $remove } = usePages();
  const { $create } = useQueries();
  const page = useOnePage(pageID || "");

  const onSaveTitle = (e: FocusEvent<HTMLInputElement>) => {
    if (!page) return;

    $update(page.id, { ...page, title: e.target.value || "Untitled" });
  };

  const onSaveDescription = (e: FocusEvent<HTMLTextAreaElement>) => {
    if (!page) return;

    $update(page.id, { ...page, description: e.target.value });
  };

  const onDeletePage = () => {
    if (!page) return;

    if (confirm(`Delete page - ${page.title}`)) {
      $remove(page.id);
    }
  };

  const onAddQuery = () => {
    if (!page) return;

    const newQuery = $create(page.id);
    focusTo(newQuery.id);
  };

  const onCopyPath = () => {
    try {
      const path = document.location.href;
      void navigator.clipboard.writeText(path);
    } catch {
      console.log("failed to copy path");
    }
  };

  // shortcuts
  useEffect(() => {
    const _s = keyboardEvent.subscribe((e) => {
      if (isHotkey("shift+a", e)) {
        e.preventDefault();
        onAddQuery();
      }

      if (isHotkey("ctrl+c", e)) {
        onCopyPath();
      }
    });

    return () => _s.unsubscribe();
  }, [pageID]);

  // update input values on page change
  const titleRef = useRef<HTMLInputElement>(null);
  const descRef = useRef<HTMLTextAreaElement>(null);
  useEffect(() => {
    if (page) {
      if (titleRef.current) {
        titleRef.current.value = page.title;
      }
      if (descRef.current) {
        descRef.current.value = page.description;
      }
    }
  }, [page]);

  useEffect(() => {
    if (page) {
      document.title = page.title || "Untitled";
    }
  }, [page]);

  if (!page) return null;

  return (
    <FramePage ref={pageRef}>
      <HeadingInput
        ref={titleRef}
        placeholder="Page title"
        onBlur={onSaveTitle}
      />
      <DescriptionArea
        ref={descRef}
        className="mt-4"
        placeholder="Page description / question this page answers"
        onBlur={onSaveDescription}
      />
      <RowSpaced>
        <Row className="mt-8">
          <Button onClick={onAddQuery}>
            <Para>Add query</Para>
            <ParaMono className="ml-8">shift+A</ParaMono>
          </Button>
          <Button className="ml-8" onClick={onCopyPath}>
            <Para>Copy page</Para>
            <ParaMono className="ml-8">ctrl+c</ParaMono>
          </Button>
          <Row className="ml-8">
            <Para className="ml-8">Press </Para>
            <KeyboardShortcut className="ml-8"> / </KeyboardShortcut>
            <Para className="ml-8"> Jump to query</Para>
          </Row>
        </Row>

        <Button className="ml-8" onClick={onDeletePage}>
          <Para>Delete</Para>
        </Button>
      </RowSpaced>
      <hr className="mt-16" />
      <div className="mt-16">
        <Query pageID={page.id} />
      </div>
    </FramePage>
  );
});

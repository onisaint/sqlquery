import {
  FC,
  ReactNode,
  createContext,
  useContext,
  useMemo,
  useState,
} from "react";

interface IFocusQueryCTX {
  queryId: number;
  focusTo: (queryId: number) => void;
  didFocus: () => void;
}

const FocusQueryCtx = createContext({} as IFocusQueryCTX);

interface IProps {
  children: ReactNode;
}

export const FocusQueryProvider: FC<IProps> = ({ children }) => {
  const [queryId, setQueryId] = useState(-1);

  const value: IFocusQueryCTX = useMemo(
    () => ({
      queryId,
      focusTo: (queryId: number) => setQueryId(queryId),
      didFocus: () => setQueryId(-1),
    }),
    [queryId]
  );

  return (
    <FocusQueryCtx.Provider value={value}>{children}</FocusQueryCtx.Provider>
  );
};

export const useFocusQuery = () => useContext(FocusQueryCtx);

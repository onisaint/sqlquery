import { observer } from "mobx-react-lite";
import { FC, lazy, useMemo } from "react";
import { useOnePage } from "../../../context/store";
import { IPage } from "../../../models/page";
import { QueryBlock } from "./block";
import { ParaMono, Small, SmallHeading } from "../../../components/types";
import { QueryGrid, QueryMapList } from "./components";
import { useFocusQuery } from "../context";

const QuerySwitcher = lazy(() => import("./switcher"));

interface IProps {
  pageID: string;
}

export const Query: FC<IProps> = observer(({ pageID }) => {
  const { focusTo } = useFocusQuery();

  const { queries } = useOnePage(pageID) as IPage;

  const queryArray = useMemo(() => Array.from(queries.values()), [queries]);

  const onFocusQuery = (queryId: number) => () => focusTo(queryId);

  return (
    <QueryGrid>
      {queryArray.length === 0 && <Small>This page has no queries</Small>}
      <div id="list">
        {queryArray.map((q) => (
          // array won't re-render on the same keys
          <QueryBlock key={`${pageID}-${q.id}`} pageID={pageID} query={q} />
        ))}
      </div>
      <div id="map">
        <div id="sticky-map">
          <SmallHeading>Queries</SmallHeading>
          {queryArray.map((q) => (
            <QueryMapList
              key={`${pageID}-${q.id}`}
              onClick={onFocusQuery(q.id)}
            >
              <ParaMono>{q.query || q.name}</ParaMono>
            </QueryMapList>
          ))}
        </div>
      </div>
      <QuerySwitcher pageID={pageID} />
    </QueryGrid>
  );
});

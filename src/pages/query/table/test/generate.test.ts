import { expect, test } from "vitest";
import {
  TStubRow,
  getARandomTable,
  getRandomNumber,
  makeTableGenerator,
} from "../generate";

/**
 * a range generator test
 * for the given range between a..b
 * "sample test for 10 folds of the range appears as a bell curve˝˝
 */
test("getRandomNumber(min, max) should return between min & max - 1", () => {
  const rangeOutput: number[] = [];
  const min = 4;
  const max = 8;

  for (let i = 0; i < 1000; i++) {
    rangeOutput.push(getRandomNumber(min, max));
  }

  // min
  expect(Math.min(...rangeOutput)).toBe(min);

  // max
  expect(Math.max(...rangeOutput)).toBe(max - 1);
});

/**
 * generate rows and check the values remains the same
 */
test("makeTableGenerator(header) should generate table values relating to its header", () => {
  const header = getARandomTable();

  const table = makeTableGenerator(header);
  const rows = [];

  for (let i = 0; i < 10; i++) {
    rows.push(...(table.next().value as TStubRow[]));
  }

  expect(rows.every((r) => r.length === header.length)).toBeTruthy();
});

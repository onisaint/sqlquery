import { FC, useEffect, useState } from "react";
import { Small } from "../../../../components/types";
import { InfiniteTableRows } from "./infiniteTable";
import { useTableData } from "./context";
import { TableFooter } from "./components";
import { Row } from "../../../../components/flex";

export const QueryTableView: FC = () => {
  const { query, data, isTableUpdating } = useTableData();

  const [lastUpdated, setLastUpdate] = useState(getRelativeTime());

  useEffect(() => {
    setLastUpdate(getRelativeTime());
  }, [query]);

  return (
    <div className="mt-8">
      <InfiniteTableRows />
      <TableFooter className="mt-8">
        <Row>
          <Small>Updated on {lastUpdated}</Small>
          {isTableUpdating && <Small className="ml-8">fetching rows...</Small>}
        </Row>
        <Small>{data.length} / 1e6 rows</Small>
      </TableFooter>
    </div>
  );
};

function getRelativeTime() {
  return new Intl.DateTimeFormat("en-GB", {
    dateStyle: "medium",
    timeStyle: "medium",
  })
    .format(new Date())
    .toString();
}

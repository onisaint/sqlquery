import { customAlphabet, nanoid } from "nanoid";

const _randomPhone = customAlphabet("0123456789", 8);

const _getRandomString = () =>
  nanoid(Math.floor((Math.random() * 10) % 12) + 4);
const _getDate = () => new Date().toISOString();
const _getAge = () => Math.floor(Math.random() * 99);
const _getPhone = () => `91${_randomPhone()}`;
const _getProgress = () => Math.floor(Math.random() * 100);
const _getStatus = () =>
  ["pending", "queue", "done"][Math.floor((Math.random() * 10) % 3)];

const GENERATE = {
  id: () => nanoid(12),
  random: _getRandomString,
  date: _getDate,
  age: _getAge,
  phone: _getPhone,
  progress: _getProgress,
  status: _getStatus,
};

export type TStubRow = (string | number)[];

export interface TStubData {
  key: string;
  title: string;
  type: keyof typeof GENERATE;
  size?: number;
}

/**
 * spit out 1000 random generated object
 *
 * @param stub
 * @returns a generator fn,
 */
export const makeTableGenerator = (stub: TStubData[]) => {
  const keys = Object.values(stub).map((v) => v.type);

  function* _generator() {
    while (true) {
      const data: TStubRow[] = [];
      for (let i = 0; i !== 1000; i++) {
        const _fake = keys.map((k) => GENERATE[k]());
        data.push(_fake);
      }

      yield data;
    }
  }

  return _generator();
};

const _definedQueriesTypes: TStubData[] = [
  { key: "gen_id", title: "password", type: "random" },
  { key: "age", title: "Age", type: "age" },
  { key: "status", title: "Status", type: "status" },
  { key: "callee", title: "Callee", type: "phone" },
  { key: "caller", title: "Called by", type: "phone" },
  { key: "user", title: "User phone", type: "phone" },
  { key: "Progress", title: "Progress", type: "progress" },
  { key: "created_at", title: "Created At", type: "date", size: 200 },
  { key: "updated_at", title: "Updated At", type: "date", size: 200 },
  { key: "deleted_at", title: "Deleted At", type: "date", size: 200 },
];

/**
 * generate a random stub table query from defined types
 *
 * @returns a random generated queries stub
 */
export const getARandomTable = (): TStubData[] => {
  const maxQueries = getRandomNumber(2, _definedQueriesTypes.length);

  const tableColumns: TStubData[] = [];
  for (let i = 0; i < maxQueries; i++) {
    tableColumns.push(_definedQueriesTypes[i]);
  }

  tableColumns.sort(() => Math.random() - 0.5);

  return tableColumns;
};

/**
 * @returns a random number between max and min
 */
export function getRandomNumber(min: number, max: number) {
  return Math.floor(Math.random() * (max - min) + min);
}

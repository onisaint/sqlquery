import {
  FC,
  HTMLAttributes,
  TableHTMLAttributes,
  useEffect,
  useRef,
} from "react";
import { useVirtual } from "react-virtual";
import { useTableData } from "./context";
import { TableBlock, TableContainer } from "./components";
import {
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";

const Row: FC<{
  index: number;
  style?: HTMLAttributes<TableHTMLAttributes<StyleSheet>>["style"];
}> = ({ index: i, style }) => {
  const { data, loadMore, isLoading } = useTableData();

  useEffect(() => {
    if (!isLoading && i === data.length - 10) {
      loadMore();
    }
  }, [isLoading]);

  const v = data[i];

  return (
    <tr style={style} key={`${v[0]}-${i}`}>
      <td>{i + 1}</td>
      {v.map((ve, j) => (
        <td key={`${v[0]}-${i}-${j}`}>{ve}</td>
      ))}
    </tr>
  );
};

export const InfiniteTableRows: FC = () => {
  const { data, header, query } = useTableData();

  const containerRef = useRef<HTMLDivElement>(null);

  const table = useReactTable({
    data,
    columns: header.map(({ key, title, size }) => ({
      accessorKey: key,
      header: title,
      size,
    })),
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
  });

  const { rows } = table.getRowModel();

  const rowVirtualizer = useVirtual({
    parentRef: containerRef,
    size: rows.length,
    overscan: 10,
  });

  const { virtualItems: virtualRows, totalSize } = rowVirtualizer;

  const paddingTop = virtualRows.length > 0 ? virtualRows?.[0]?.start || 0 : 0;
  const paddingBottom =
    virtualRows.length > 0
      ? totalSize - (virtualRows?.[virtualRows.length - 1]?.end || 0)
      : 0;

  useEffect(() => {
    if (query !== "") {
      containerRef.current?.scrollTo({ top: 1 });
    }
  }, [query]);

  return (
    <TableContainer ref={containerRef}>
      <TableBlock>
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              <th>Nu.</th>
              {headerGroup.headers.map((header) => {
                return (
                  <th
                    key={header.id}
                    colSpan={header.colSpan}
                    style={{ width: header.getSize() }}
                  >
                    {flexRender(
                      header.column.columnDef.header,
                      header.getContext()
                    )}
                  </th>
                );
              })}
            </tr>
          ))}
        </thead>
        <tbody>
          {paddingTop > 0 && (
            <tr>
              <td style={{ height: paddingTop, border: "none" }} />
            </tr>
          )}
          {rowVirtualizer.virtualItems.map(({ index, key }) => (
            <Row key={key} index={index} />
          ))}
          {paddingBottom > 0 && (
            <tr>
              <td style={{ height: paddingBottom, border: "none" }} />
            </tr>
          )}
        </tbody>
      </TableBlock>
    </TableContainer>
  );
};

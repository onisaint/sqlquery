import { styled } from "styled-components";
import { RowSpaced } from "../../../../components/flex";

export const TableContainer = styled.div`
  max-height: 200px;
  overflow: auto;
  border: 1px solid #f1f1f1;
  border-radius: 3px;

  @media (min-width: 1000px) {
    min-width: 618px;
    max-width: 618px;
  }

  @media (min-width: 1400px) {
    min-width: 1018px;
    max-width: 1018px;
  }
`;

export const TableBlock = styled.table`
  padding: 0;
  border-collapse: collapse;
  border-spacing: 0;
  table-layout: fixed;
  width: max-content;

  th,
  td {
    border: 1px solid #f1f1f1;
    border-collapse: collapse;
  }

  & > thead {
    margin: 0;
    position: sticky;
    top: -1px;
    z-index: 2;
    max-height: 25px;
    & > tr {
      & > th {
        padding: 8px 6px;
        background: #fafafa;
        text-align: left;
        font-size: var(--font-label);
        font-family: var(--sans);
        font-weight: bold;
        color: #424242;
        min-width: 100px;
        text-transform: capitalize;

        &:first-child {
          min-width: 40px;
          max-width: 40px;
        }
      }
    }
  }

  & > tbody {
    width: 100%;

    & > tr {
      width: 100%;
    }
  }

  td {
    font-size: var(--font-small);
    font-family: var(--sans);
    font-weight: 300;
    color: #424242;
    min-width: 100px;
    width: fit-content;
    padding: 6px;

    &:first-child {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      min-width: 40px;
      max-width: 40px;
      color: #a1a1a1;
    }
  }
`;

export const TableFooter = styled(RowSpaced)`
  & > * {
    color: #a1a1a1;
  }
`;

import {
  FC,
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useMemo,
  useRef,
  useState,
  useTransition,
} from "react";
import {
  TStubData,
  TStubRow,
  getARandomTable,
  makeTableGenerator,
} from "./generate";

interface ITableCTX {
  query: string;
  header: TStubData[];
  data: TStubRow[];
  isLoading: boolean;
  isTableUpdating: boolean;
  loadMore: () => void;
}

const TableCtx = createContext({} as ITableCTX);

interface IProps {
  query: string;
  runId: number;
  children: ReactNode;
}

export const TableProvider: FC<IProps> = ({ query, runId, children }) => {
  const [isUpdating, startTransition] = useTransition();

  const tableData = useRef<TStubRow[]>([]);

  const [isLoading, setLoading] = useState(false);

  const [header, table] = useMemo(() => {
    if (query === "") return [[], null];

    const _header = getARandomTable();
    const _table = makeTableGenerator(_header);

    startTransition(() => {
      tableData.current = _table.next().value as TStubRow[];
    });

    return [_header, _table];
  }, [query, runId]);

  const loadMoreData = useCallback(() => {
    if (table === null) return;

    if (!isLoading) {
      setLoading(true);
      startTransition(() => {
        tableData.current = tableData.current.concat(
          table.next().value as TStubRow[]
        );

        setLoading(false);
      });
    }
  }, [table, isLoading]);

  const value: ITableCTX = useMemo(
    () => ({
      isLoading,
      query,
      header,
      data: tableData.current,
      isTableUpdating: isUpdating,
      loadMore: loadMoreData,
    }),
    [header, tableData, query, isLoading, isUpdating]
  );

  if (query === "") {
    return null;
  }

  return <TableCtx.Provider value={value}>{children}</TableCtx.Provider>;
};

export const useTableData = () => useContext(TableCtx);

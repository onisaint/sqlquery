import { FC, useEffect, useState } from "react";
import { TableProvider } from "./context";
import { QueryTableView } from "./view";
import { useInView } from "react-intersection-observer";
import { Small } from "../../../../components/types";

interface IProps {
  // the actual query
  query: string;
  // the run id (emulates save and changes table)
  runId: number;
}

const Table: FC<IProps> = ({ query, runId }) => {
  const { ref, inView } = useInView();

  const [show, setShow] = useState(inView);

  useEffect(() => {
    if (inView) {
      setShow(true);
    }
  }, [inView]);

  return (
    <>
      <div ref={ref} />
      {show ? (
        <TableProvider query={query} runId={runId}>
          <QueryTableView />
        </TableProvider>
      ) : (
        <Small className="mt-8">Loading query...</Small>
      )}
    </>
  );
};

export default Table;

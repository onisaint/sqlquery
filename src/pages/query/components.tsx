import { styled } from "styled-components";
import { Row } from "../../../components/flex";
import { Input, SQLInput } from "../../../components/input";
import { Button } from "../../../components/button";

export const QueryGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 150px;
  grid-template-areas: "list map";
  padding-bottom: 200px;
  position: relative;

  & > #list {
    grid-area: list;
    width: 100%;
  }

  & > #map {
    grid-area: map;
    max-width: 150px;
    margin-left: 16px;

    & > #sticky-map {
      position: sticky;
      top: 16px;
    }
  }
`;

export const QueryContainer = styled(Row)`
  align-items: flex-start;
  height: fit-content;
  position: relative;
`;

export const QueryOpBlock = styled.div`
  width: 30px;
  padding: 4px;
  background: #f1f1f1;
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  left: -40px;
  border-radius: 7px;
`;

export const QueryViewBlock = styled.div`
  width: 100%;
  height: fit-content;
`;

export const QueryNameInput = styled(Input)`
  font-size: var(--font-para);
`;

export const QuerySQLInput = styled(SQLInput)`
  background: #fff8e1;

  &:hover,
  &:focus {
    background: #ffecb3;
  }
`;

export const QueryMapList = styled(Button)`
  padding: 0;
  background: transparent;
  font-family: var(--mono);
  font-size: var(--font-para);
  margin-top: 8px;
  opacity: 0.7;
  transition: opacity 0.17s ease;

  &:hover,
  &:focus {
    opacity: 1;
    background: transparent;
  }
`;

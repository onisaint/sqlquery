import { FC, useEffect, useMemo, useState } from "react";
import { observer } from "mobx-react-lite";
import { useOnePage } from "../../../../context/store";
import { IPage } from "../../../../models/page";
import { Para, ParaMono, Small } from "../../../../components/types";
import { useKeyboardEvents } from "../../../../context/keyboard";
import isHotkey from "is-hotkey";
import {
  CMDDialog,
  CMDInput,
  CMDItem,
  CMDList,
  CommandContainer,
} from "./component";
import { useFocusQuery } from "../../context";

interface IProps {
  pageID: string;
}

const QuerySwitcher: FC<IProps> = observer(({ pageID }) => {
  const keyboardEvent = useKeyboardEvents();

  const { focusTo } = useFocusQuery();

  const { queries } = useOnePage(pageID) as IPage;

  const [open, setOpen] = useState(false);

  const queryArray = useMemo(() => Array.from(queries.values()), [queries]);

  const onFocusQuery = (queryId: number) => () => {
    focusTo(queryId);
    setOpen(false);
  };

  useEffect(() => {
    const _s = keyboardEvent.subscribe((e) => {
      if (isHotkey("/", e)) {
        setOpen(true);
        e.preventDefault();
      }
    });

    return () => _s.unsubscribe();
  }, [pageID]);

  return (
    <CommandContainer>
      <CMDDialog open={open} onOpenChange={setOpen}>
        <CMDInput placeholder="Search query by name or sql" />
        {queryArray.length === 0 && (
          <Small>No queries. Add and come back</Small>
        )}
        {queryArray.length !== 0 && (
          <>
            <Small className="mt-8 pl-4">Queries</Small>
            <CMDList className="mt-4">
              {queryArray.map((q) => (
                <CMDItem
                  className="mt-8"
                  value={`${q.name}${q.query}`}
                  key={`${pageID}-${q.id}`}
                  onSelect={onFocusQuery(q.id)}
                >
                  <Para>{q.name}</Para>
                  <ParaMono className="mt-4">{q.query}</ParaMono>
                </CMDItem>
              ))}
            </CMDList>
          </>
        )}
      </CMDDialog>
    </CommandContainer>
  );
});

export default QuerySwitcher;

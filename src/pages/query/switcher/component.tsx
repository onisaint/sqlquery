import { Command } from "cmdk";
import { styled } from "styled-components";
import { InputStyles } from "../../../../components/input";

export const CommandContainer = styled.div``;

export const CMDDialog = styled(Command.Dialog)`
  inset: 0;
  background: white;
  border-radius: 3px;
  box-shadow: hsl(206 22% 7% / 35%) 0px 10px 38px -10px,
    hsl(206 22% 7% / 20%) 0px 10px 20px -15px;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -75%);
  width: 50vw;
  max-width: 500px;
  max-height: 400px;
  height: 375px;
  padding: 8px;
  animation: contentShow 150ms cubic-bezier(0.16, 1, 0.3, 1);
  z-index: 4;
`;

export const CMDInput = styled(Command.Input)`
  ${InputStyles};
  font-size: var(--font-para);
`;

export const CMDList = styled(Command.List)`
  -ms-scroll-chaining: none;
  overscroll-behavior: contain;
  scroll-padding-block-end: 40px;
  transition: 0.1s ease;
  transition-property: height;
  overflow: scroll;
  height: 300px;
`;

export const CMDItem = styled(Command.Item)`
  cursor: pointer;
  padding: 4px 6px;
  border-bottom: 1px solid #f2f2f2;
  border-radius: 3px;

  &[data-selected] {
    background: #f1f1f1;
  }
`;

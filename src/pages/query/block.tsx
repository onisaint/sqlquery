import { observer } from "mobx-react-lite";
import {
  FC,
  FocusEvent,
  KeyboardEvent,
  Suspense,
  lazy,
  useEffect,
  useRef,
  useState,
  useTransition,
} from "react";
import { IQuery } from "../../../models/query";
import {
  QueryContainer,
  QueryNameInput,
  QueryOpBlock,
  QuerySQLInput,
  QueryViewBlock,
} from "./components";
import { Small } from "../../../components/types";
import { Button } from "../../../components/button";
import { useQueries } from "../../../context/store";
import { useFocusQuery } from "../context";

const QueryTable = lazy(() => import("./table"));

interface IProps {
  pageID: string;
  query: IQuery;
}

export const QueryBlock: FC<IProps> = observer(({ pageID, query }) => {
  const [, startTransition] = useTransition();

  const { queryId: focusToQueryId, focusTo, didFocus } = useFocusQuery();

  const { $create, $remove, $update } = useQueries();

  const queryTitleRef = useRef<HTMLInputElement>(null);
  const queryInputRef = useRef<HTMLTextAreaElement>(null);

  const [runId, setRunId] = useState(Date.now());

  const onAddQuery = () => {
    const newQuery = $create(pageID, query.id);
    focusTo(newQuery.id);
  };

  const onDeleteQuery = () => {
    if (confirm(`Delete query - ${query.name}`)) {
      $remove(pageID, query.id);
    }
  };

  const onSaveName = (e: FocusEvent<HTMLInputElement>) => {
    const name = e.target.value || `query-${query.id}`;
    $update(pageID, query.id, {
      ...query,
      name,
    });

    if (queryTitleRef.current) {
      queryTitleRef.current.value = name;
    }
  };

  const onSaveQuery = (e: FocusEvent<HTMLTextAreaElement>) => {
    $update(pageID, query.id, { ...query, query: e.target.value });
    setRunId(Date.now());
  };

  const onQueryKeyDown = (e: KeyboardEvent<HTMLTextAreaElement>) => {
    if (queryInputRef.current && e.shiftKey && e.key === "Enter") {
      e.preventDefault();
      $update(pageID, query.id, {
        ...query,
        query: queryInputRef.current.value,
      });
      setRunId(Date.now());
    }
  };

  // handle focus to query
  useEffect(() => {
    if (focusToQueryId === query.id) {
      startTransition(() => {
        if (queryTitleRef.current) {
          queryTitleRef.current.focus();
          queryTitleRef.current.scrollIntoView({
            behavior: "smooth",
            block: "nearest",
          });
          didFocus();
        }
      });
    }
  }, [query, focusToQueryId]);

  return (
    <div className="mb-16">
      <QueryContainer>
        <QueryOpBlock>
          <Button
            className="mt-4"
            onClick={onDeleteQuery}
            aria-label="delete query"
          >
            <Small>🗑️</Small>
          </Button>
          <Button
            className="mt-16"
            onClick={onAddQuery}
            aria-label="Add a query before"
          >
            <Small>➕</Small>
          </Button>
        </QueryOpBlock>
        <QueryViewBlock>
          <QueryNameInput
            ref={queryTitleRef}
            defaultValue={query.name}
            placeholder="What does this query do?"
            onBlur={onSaveName}
          />
          <QuerySQLInput
            ref={queryInputRef}
            defaultValue={query.query}
            className="mt-4"
            placeholder="sql query. Press shift+enter to execute inline"
            onBlur={onSaveQuery}
            onKeyDown={onQueryKeyDown}
          />
          <Suspense fallback={<Small>Loading...</Small>}>
            <QueryTable query={query.query} runId={runId} />
          </Suspense>
        </QueryViewBlock>
      </QueryContainer>
    </div>
  );
});

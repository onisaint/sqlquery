import { onCLS } from "web-vitals/onCLS.js";
import { onFCP } from "web-vitals/onFCP.js";
import { onFID } from "web-vitals/onFID.js";
import { onINP } from "web-vitals/onINP.js";
import { onLCP } from "web-vitals/onLCP.js";
import { onTTFB } from "web-vitals/onTTFB.js";

const log = () => {
  onCLS(console.log);
  onFID(console.log);
  onLCP(console.log);
  onINP(console.log);
  onFCP(console.log);
  onTTFB(console.log);
};

export default log;
